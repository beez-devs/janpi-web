﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class TipoDocumento : Auditoria
    {
        public long idTipoDocumento { get; set; }
        public string strTipoDocumento { get; set; }
        public int intLongitud { get; set; }
    }
}
