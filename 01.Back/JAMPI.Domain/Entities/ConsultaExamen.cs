﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class ConsultaExamen : Auditoria
    {
        public long idConsultaExamen { get; set; }
        public long idConsulta { get; set; }
        public long idExamen { get; set; }
        public string strDescripcion { get; set; }
    }
}
