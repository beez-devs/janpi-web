﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class UsuarioLogin
    {
        public int intResultado { get; set; }
        public Session obeSession { get; set; }
        public List<Menu> lbeMenu { get; set; }

        public UsuarioLogin()
        {
            intResultado = -1;
            obeSession = new Session();
            lbeMenu = new List<Menu>();
        }
    }
}
