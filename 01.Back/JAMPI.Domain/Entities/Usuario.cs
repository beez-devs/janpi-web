﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Entities
{
    public class Usuario : Persona
    {
        //public long idUsuario { get; set; }
        //public string strClave { get; set; }
        public long idUsuario { get; set; }
        public string strNombreUsuario { get; set; }
        public string strClave { get; set; }
        public string strFechaCaducidad { get; set; }
        public long idUsuarioPadre { get; set; }


    }
}
