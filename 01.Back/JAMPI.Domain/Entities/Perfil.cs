﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Perfil
    {
        public long idPerfil { get; set; }
        public string strPerfil { get; set; }
    }
}
