﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Entities
{
    public class Session
    {
        public long idPerfil { get; set; }
        public string strPerfil { get; set; }
        public long idUsuario { get; set; }
        public long idCliente { get; set; }
        public long idPersonal { get; set; }
        public string strUsuario { get; set; }
        public string strNombres { get; set; }
        public string strApellidoPaterno { get; set; }
        public string strApellidoMaterno { get; set; }
        public string strDocumentoIdentidad { get; set; }
        public string strRutaFoto { get; set; }
        public string strCargo { get; set; }
        public long idCargo { get; set; }
    }
}
