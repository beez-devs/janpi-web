﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Domain.Commons
{
    public class Constants
    {
        #region "[Generico]"
        public static string _GENERAL_GET_OK = "SE RECUPERÓ LA INFORMACIÓN CORRECTAMENTE";
        public static string _GENERAL_LIST_OK = "SE LISTÓ LA INFORMACIÓN CORRECTAMENTE";
        public static string _GENERAL_INSERT_OK = "SE GUARDÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_UPDATE_OK = "SE ACTUALIZÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_DELETE_OK = "SE BORRÓ EL REGISTRO CORRECTAMENTE";
        public static string _GENERAL_GET_ERROR = "NO SE PUDO OBTENER LA INFORMACIÓN";
        public static string _GENERAL_LIST_ERROR = "NO SE PUDO LISTAR LA INFORMACIÓN";
        public static string _GENERAL_INSERT_ERROR = "NO SE PUDO PROCESAR EL PEDIDO";
        public static string _GENERAL_UPDATE_ERROR = "ERROR: {0}";
        public static string _GENERAL_DELETE_ERROR = "ERROR: {0}";
        public static string _GENERAL_ERROR = "NO SE PUDO CONECTAR A LA BASE DE DATOS";
        public static string _GENERAL_ERROR_DEV = "ERROR GENERAL: {0}";
        public static string _GENERAL_ERROR_SQL = "ERROR SQL {0}: {1}";
        #endregion

        #region "[Persona]"
        public static string _PERSONA_EXISTE = "YA EXISTE UNA CUENTA CON EL CORREO ELECTRÓNICO INGRESADO";
        #endregion

        #region "[CAPTCHA]"
        public static string _CAPTCHA_VALIDATION_ERROR = "NO SE PUDO VALIDAR EL CAPTCHA";
        public static string _CAPTCHA_VALIDATION_ERROR_DEV = "ERROR: {0}";
        #endregion

        #region "[Login]"
        public const string _LOGIN_NO_EXISTE = "EL USUARIO NO ESTA REGISTRADO EN EL SISTEMA.";
        public const string _LOGIN_NO_EXISTE_DEV = "RETORNO USUARIO NULL.";
        #endregion
    }
}
