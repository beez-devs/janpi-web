﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Commons
{
    public abstract class Auditoria
    {
        public string strFechaRegistro {get;set;}
        public string strFechaModifica { get; set; }
        public string strUsuarioModifica { get; set; }
        public long idUsuarioModifica { get; set; }
        public string strFechaElimina { get; set; }
        public string strUsuarioElimina { get; set; }
        public long idUsuarioElimina { get; set; }
        public string strEstado { get; set; }
    }
}
