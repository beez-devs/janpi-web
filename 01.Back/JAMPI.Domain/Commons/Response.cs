﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Domain.Commons
{
    public class Response<T>
    {
        public string message { get; set; }
        public bool success { get; set; }
        public T data { get; set; }
        public string developer { get; set; }
        public int type { get; set; }
    }
}
