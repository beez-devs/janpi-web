﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JANPI.Infrasturcture.ORM
{
    public class Base
    {
        class Objeto
        {
            public int posicion { get; set; }
            public string nombre { get; set; }
            public string nombreObj { get; set; }
        }

        class ObjetoPropiedad
        {
            public int posicion { get; set; }
            public string nombre { get; set; }
            public PropertyInfo oPropiedad { get; set; }
        }

        public class ParametroSQL
        {
            public string strNombre { get; set; }
            public SqlDbType tipo { get; set; }
            public ParameterDirection direccion { get; set; }
            public int intTamanio { get; set; }
            public object valor { get; set; }
            public ParametroSQL(string _strNombre, SqlDbType _tipo, object _valor, ParameterDirection _direccion = ParameterDirection.Input, int _Tamanio = 0)
            {
                this.strNombre = _strNombre;
                this.tipo = _tipo;
                if (_tipo == SqlDbType.VarChar)
                    this.valor = _valor != null ? _valor.ToString().ToUpper() : "";
                else if (_tipo == SqlDbType.DateTime)
                    this.valor = _valor != null ? DateTime.Parse(_valor.ToString(), new CultureInfo("es-PE")) : _valor;
                else if (_tipo == SqlDbType.Decimal)
                    this.valor = _valor != null ? Decimal.Parse(_valor.ToString(), new CultureInfo("es-PE")) : _valor;
                else
                    this.valor = _valor;
                this.direccion = _direccion;
                this.intTamanio = _Tamanio;
            }


        }


        internal SqlParameter Add(string Parametro, object Valor, SqlDbType Tipo, ParameterDirection Direccion = ParameterDirection.Input, int Tamanio = 0)
        {
            SqlParameter par = new SqlParameter(Parametro, Tipo, Tamanio);
            par.Direction = Direccion;
            par.Value = Valor;
            return par;
        }

        internal SqlDataReader ExecuteReader(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null, CommandBehavior tipoComando = CommandBehavior.Default)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            return cmd.ExecuteReader(tipoComando);
        }

        internal async Task<SqlDataReader> ExecuteReaderAsync(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null, CommandBehavior tipoComando = CommandBehavior.Default)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            return await cmd.ExecuteReaderAsync(tipoComando);
        }

        internal object ExecuteScalar(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            return cmd.ExecuteScalar();
        }

        internal T ExecuteScalar<T>(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            return (T)cmd.ExecuteScalar();
        }

        internal int ExecuteNonQuery(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            return cmd.ExecuteNonQuery();
        }

        internal async Task<ResponseSQL> ExecuteNonQueryAsync(string strProcedimiento, List<ParametroSQL> lParametros, SqlConnection con, SqlTransaction trx = null)
        {
            SqlCommand cmd = new SqlCommand(strProcedimiento, con, trx);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sqlP;
            foreach (ParametroSQL par in lParametros)
            {
                sqlP = new SqlParameter(par.strNombre, par.tipo, par.intTamanio);
                sqlP.Direction = par.direccion;
                sqlP.Value = par.valor;
                cmd.Parameters.Add(sqlP);
            }
            await cmd.ExecuteNonQueryAsync();
            return new ResponseSQL()
            {
                resultado = int.Parse(cmd.Parameters["@PO_RESULTADO"].Value.ToString()),
                mensaje = cmd.Parameters["@PO_RESULTADOMSG"].Value.ToString()
            };
        }

        internal object ObtenerScalar(SqlDataReader drd)
        {
            if (drd.HasRows)
            {
                drd.Read();
                return drd.GetValue(0);
            }
            else
            {
                return null;
            }
        }

        internal T ObtenerObjeto<T>(SqlDataReader drd)
        {
            T obj = (T)Activator.CreateInstance(typeof(T));
            if (drd.HasRows)
            {
                List<Objeto> lObjetos = new List<Objeto>();
                Objeto o = null;
                for (int i = 0; i < drd.FieldCount; i++)
                {
                    o = new Objeto();
                    o.posicion = drd.GetOrdinal(drd.GetName(i));
                    o.nombre = drd.GetName(i).ToUpper();
                    o.nombreObj = ValidaNombre(drd.GetName(i).ToUpper());
                    lObjetos.Add(o);
                }

                List<PropertyInfo> propiedades = obj.GetType().GetProperties().ToList();
                PropertyInfo oFind;


                //LEO EL DATAREADER PARA OBTENER OBJETOS
                drd.Read();


                for (int i = 0; i < lObjetos.Count; i++)
                {
                    oFind = propiedades.Find(x => x.Name.ToUpper() == lObjetos[i].nombreObj.ToUpper());
                    if (oFind != null)
                    {
                        if (lObjetos[i].nombre.StartsWith("TX"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetString(lObjetos[i].posicion) : "");
                        else if (lObjetos[i].nombre.StartsWith("FG"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetString(lObjetos[i].posicion) : "");
                        else if (lObjetos[i].nombre.StartsWith("NU"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetInt32(lObjetos[i].posicion) : 0);
                        else if (lObjetos[i].nombre.StartsWith("ID"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetInt64(lObjetos[i].posicion) : 0);
                        else if (lObjetos[i].nombre.StartsWith("FE"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetDateTime(lObjetos[i].posicion).ToString("dd/MM/yyyy") : "");
                        else if (lObjetos[i].nombre.StartsWith("DE"))
                            oFind.SetValue(obj, (!drd.IsDBNull(lObjetos[i].posicion)) ? drd.GetDecimal(lObjetos[i].posicion) : 0);
                    }
                }
            }
            else
            {
                obj = default(T);
            }
            return obj;
        }



        internal List<T> ListarObjeto<T>(SqlDataReader drd)
        {
            List<T> list = (List<T>)Activator.CreateInstance(typeof(List<T>));
            T obj = (T)Activator.CreateInstance(typeof(T));
            if (drd.HasRows)
            {
                List<Objeto> lObjetos = new List<Objeto>();
                Objeto o = null;
                for (int i = 0; i < drd.FieldCount; i++)
                {
                    o = new Objeto();
                    o.posicion = drd.GetOrdinal(drd.GetName(i));
                    o.nombre = drd.GetName(i).ToUpper();
                    o.nombreObj = ValidaNombre(drd.GetName(i).ToUpper());
                    lObjetos.Add(o);
                }

                PropertyInfo[] propiedades = obj.GetType().GetProperties();
                Objeto oFind;

                List<ObjetoPropiedad> lObjetoPropiedad = new List<ObjetoPropiedad>();
                ObjetoPropiedad objPropiedad = null;
                for (int i = 0; i < propiedades.Length; i++)
                {
                    oFind = lObjetos.Find(x => x.nombreObj.ToUpper() == propiedades[i].Name.ToUpper());
                    if (oFind != null)
                    {
                        objPropiedad = new ObjetoPropiedad();
                        objPropiedad.oPropiedad = propiedades[i];
                        objPropiedad.posicion = oFind.posicion;
                        objPropiedad.nombre = oFind.nombre;
                        lObjetoPropiedad.Add(objPropiedad);
                    }
                }


                //LEO EL DATAREADER PARA OBTENER OBJETOS
                while (drd.Read())
                {
                    obj = (T)Activator.CreateInstance(typeof(T));
                    for (int i = 0; i < lObjetoPropiedad.Count; i++)
                    {
                        if (lObjetoPropiedad[i].nombre.StartsWith("TX"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetString(lObjetoPropiedad[i].posicion) : "");
                        else if (lObjetoPropiedad[i].nombre.StartsWith("FG"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetString(lObjetoPropiedad[i].posicion) : "");
                        else if (lObjetoPropiedad[i].nombre.StartsWith("NU"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetInt32(lObjetoPropiedad[i].posicion) : 0);
                        else if (lObjetoPropiedad[i].nombre.StartsWith("ID"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetInt64(lObjetoPropiedad[i].posicion) : 0);
                        else if (lObjetoPropiedad[i].nombre.StartsWith("FE"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetDateTime(lObjetoPropiedad[i].posicion).ToString("dd/MM/yyyy") : "");
                        else if (lObjetoPropiedad[i].nombre.StartsWith("DE"))
                            lObjetoPropiedad[i].oPropiedad.SetValue(obj, (!drd.IsDBNull(lObjetoPropiedad[i].posicion)) ? drd.GetDecimal(lObjetoPropiedad[i].posicion) : 0);
                    }
                    list.Add(obj);
                }
            }
            else
            {
                list = default(List<T>);
            }
            return list;
        }


        private string ValidaNombre(string nombre)
        {
            string ret = "";
            if (nombre.ToUpper().StartsWith("TX")) ret = "STR" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("FG")) ret = "STR" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("FE")) ret = "STRFECHA" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("NU")) ret = "INT" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("DE")) ret = "DEC" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("BL")) ret = "BL" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("ID")) ret = "ID" + nombre.ToUpper().Substring(2);
            else if (nombre.ToUpper().StartsWith("NU")) ret = "INT" + nombre.ToUpper().Substring(2);
            return ret;
        }
    }
}
