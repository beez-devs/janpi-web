﻿using JAMPI.Application.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Infrastructure.Repositories
{
    public class UnitOfWork :IUnitOfWork
    {
        public IUsuarioRepository usuarioRepository { get => new UsuarioRepository(); }
        public IPersonaRepository personaRepository { get => new PersonaRepository(); }
    }
}
