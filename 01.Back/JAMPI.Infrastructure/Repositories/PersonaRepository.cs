﻿
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using JANPI.Infrasturcture.ORM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Infrastructure.Repositories
{
    public class PersonaRepository : Base, IPersonaRepository
    {
        //public async Task<int> Add(SqlConnection con, Persona entity)
        //{
        //    int rpta = 0;
        //    List<ParametroSQL> lParametros = new List<ParametroSQL>();
        //    lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
        //    lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input, 20));
        //    lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.Char, entity.strSexo, ParameterDirection.Input, 1));
        //    lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input, 20));
        //    lParametros.Add(new ParametroSQL("@PI_TXUBIGEONACIMIENTO", SqlDbType.Char, entity.strUbigeoNacimiento, ParameterDirection.Input, 6));
        //    lParametros.Add(new ParametroSQL("@PI_TXUBIGEO", SqlDbType.Char, entity.strUbigeo , ParameterDirection.Input, 6));
        //    lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input, 500));
        //    lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input, 20));
        //    lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input, 20));
        //    lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstr, ParameterDirection.Input));
        //    lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input, 100));
        //    lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, entity.usuario.strClave, ParameterDirection.Input, 50));
        //    lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));

        //    rpta = await ExecuteNonQueryAsync("USP_PERSONA_INSERT", lParametros, con);

        //    return rpta;
        //}
        public async Task<ResponseSQL> Add(SqlConnection con, Persona entity)
        {
            ResponseSQL rpta = new ResponseSQL();
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDPARAMTIPODOCUMENTO", SqlDbType.BigInt, entity.idParamTipoDocumento, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXNUMERODOCUMENTO", SqlDbType.VarChar, entity.strNumeroDocumento, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXNOMBRE", SqlDbType.VarChar, entity.strNombre, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXPRIMERAPELLIDO", SqlDbType.VarChar, entity.strPrimerApellido, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXSEGUNDOAPELLIDO", SqlDbType.VarChar, entity.strSegundoApellido, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_FGSEXO", SqlDbType.Char, entity.strSexo, ParameterDirection.Input, 1));
            lParametros.Add(new ParametroSQL("@PI_FENACIMIENTO", SqlDbType.VarChar, entity.strFechaNacimiento, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXUBIGEONACIMIENTO", SqlDbType.Char, entity.strUbigeoNacimiento, ParameterDirection.Input, 6));
            lParametros.Add(new ParametroSQL("@PI_TXUBIGEO", SqlDbType.Char, entity.strUbigeo, ParameterDirection.Input, 6));
            lParametros.Add(new ParametroSQL("@PI_TXDIRECCION", SqlDbType.VarChar, entity.strDireccion, ParameterDirection.Input, 500));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARPRINCIPAL", SqlDbType.VarChar, entity.strCelularPrincipal, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXCELULARSECUNDARIO", SqlDbType.VarChar, entity.strCelularSecundario, ParameterDirection.Input, 20));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOPRINCIPAL", SqlDbType.VarChar, entity.strCorreoPrincipal, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_TXCORREOSECUNDARIO", SqlDbType.VarChar, entity.strCorreoSecundario, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PI_IDPARAMGRADOINSTR", SqlDbType.BigInt, entity.idParamGradoInstr, ParameterDirection.Input));
            lParametros.Add(new ParametroSQL("@PI_TXOCUPACION", SqlDbType.VarChar, entity.strOcupacion, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, entity.usuario.strClave, ParameterDirection.Input, 50));
            lParametros.Add(new ParametroSQL("@PO_RESULTADO", SqlDbType.BigInt, 0, ParameterDirection.Output));
            lParametros.Add(new ParametroSQL("@PO_RESULTADOMSG", SqlDbType.VarChar, null, ParameterDirection.Output,500));

            rpta = await ExecuteNonQueryAsync("USP_PERSONA_INSERT", lParametros, con);

            return rpta;
        }
        public Task<ResponseSQL> Delete(SqlConnection con, int id)
        {
            throw new NotImplementedException();
        }

        public async Task<Persona> Get(SqlConnection con, int id)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_IDUSUARIO", SqlDbType.BigInt, id, ParameterDirection.Input));
            SqlDataReader drd = await ExecuteReaderAsync("USP_PERSONA_ID", lParametros, con);
            Persona oPersona = ObtenerObjeto<Persona>(drd);
            drd.Close();
            return oPersona;
        }

        public Task<IEnumerable<Persona>> GetAll(SqlConnection con)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseSQL> Update(SqlConnection con, Persona entity)
        {
            throw new NotImplementedException();
        }
    }
}
