﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using JANPI.Infrasturcture.ORM;
using JAMPI.Domain.Commons;

namespace JAMPI.Infrastructure.Repositories
{
    public class UsuarioRepository : Base, IUsuarioRepository
    {
        public Task<ResponseSQL> Add(SqlConnection con, Usuario entity)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseSQL> Delete(SqlConnection con, int id)
        {
            throw new NotImplementedException();
        }

        public Task<Usuario> Get(SqlConnection con, int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Usuario>> GetAll(SqlConnection con)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_LISTAR", lParametros, con);
            IEnumerable<Usuario> oListaUsuario = ListarObjeto<Usuario>(drd);
            drd.Close();
            return oListaUsuario;
        }

        public Task<ResponseSQL> Update(SqlConnection con, Usuario entity)
        {
            throw new NotImplementedException();
        }

        public async Task<Usuario> validarLogin(SqlConnection con, LoginRequest loginRequest)
        {
            List<ParametroSQL> lParametros = new List<ParametroSQL>();
            lParametros.Add(new ParametroSQL("@PI_TXUSUARIO", SqlDbType.VarChar, loginRequest.strUsuario, ParameterDirection.Input, 100));
            lParametros.Add(new ParametroSQL("@PI_TXCLAVE", SqlDbType.VarChar, loginRequest.strPassword, ParameterDirection.Input, 100));
            SqlDataReader drd = await ExecuteReaderAsync("USP_USUARIO_VALIDAR_LOGIN", lParametros, con);
            Usuario oUsuario = ObtenerObjeto<Usuario>(drd);
            drd.Close();
            return oUsuario;
        }
    }
}
