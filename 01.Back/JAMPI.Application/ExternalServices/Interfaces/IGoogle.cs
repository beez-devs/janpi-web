﻿using JAMPI.Domain.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.ExternalServices.Interfaces
{
    public interface IGoogle
    {
        Task<Response<TokenResponse>> Verificar(string token);
    }
}
