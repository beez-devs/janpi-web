﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Contracts.Requests
{
    public class LoginRequest
    {
        public string strToken { get; set; }
        public string strUsuario { get; set; }
        public string strPassword { get; set; }
    }
}
