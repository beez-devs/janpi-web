﻿
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

using System.Threading.Tasks;

namespace JAMPI.Application.Structure.UsuarioStructure.Services
{
    public class PersonaService : IPersonaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string _connectionString;
        public PersonaService(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _connectionString = configuration.GetConnectionString("ConnJAMPI");
        }
        public async Task<Response<int>> InsertarAsync(Persona persona)
        {
            Response<int> oResponse = new Response<int>();      

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                try
                {
                    await con.OpenAsync();
                    ResponseSQL oResponseSQL = new ResponseSQL();
                    persona.usuario = new Usuario();
                    persona.usuario.strClave = "123245";
                    oResponseSQL = await _unitOfWork.personaRepository.Add(con, persona);
                    oResponse.success = false;
                    oResponse.data = oResponseSQL.resultado;
                    if (oResponseSQL.resultado == 0)
                    {
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                    }else if (oResponseSQL.resultado == 1)
                    {
                        oResponse.success = true;
                        oResponse.message = Constants._GENERAL_INSERT_OK;
                    }else if (oResponseSQL.resultado == 2)
                    {
                        oResponse.message = Constants._PERSONA_EXISTE;
                    }
                    else
                    {                      
                        oResponse.message = Constants._GENERAL_INSERT_ERROR;
                        oResponse.developer = string.Format(Constants._GENERAL_ERROR_SQL, oResponseSQL.resultado, oResponseSQL.mensaje ) ;
                        oResponse.data = 0;
                    }

                }
                catch (Exception ex)
                {
                    oResponse.success = false;
                    oResponse.developer = String.Format(oResponse.developer, ex.Message);
                }
            }
            return oResponse;
        }
    }
}
