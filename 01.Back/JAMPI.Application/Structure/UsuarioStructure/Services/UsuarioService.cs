﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Application.Interfaces.Repositories;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using JAMPI.Application.ExternalServices.Interfaces;

namespace JAMPI.Application.Structure.UsuarioStructure.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private string cn;
        private IGoogle _google;
        public UsuarioService(IConfiguration configuration, IUnitOfWork unitOfWork, IGoogle google)
        {
            _unitOfWork = unitOfWork;
            _google = google;
            cn = configuration.GetConnectionString("ConnJAMPI");
        }


        public async Task<Response<Usuario>> AccederAsync(LoginRequest loginRequest)
        {
            Response<Usuario> oResponse = new Response<Usuario>();
            //Consulta con Google
            var result = await _google.Verificar(loginRequest.strToken);
            if (result.success)
            {

                using (SqlConnection con = new SqlConnection(cn))
                {
                    try
                    {
                        await con.OpenAsync();
                        oResponse.data = await _unitOfWork.usuarioRepository.validarLogin(con, loginRequest);
                        if (oResponse.data != null)
                        {
                            oResponse.success = true;
                        }
                        else
                        {
                            oResponse.success = false;
                            oResponse.message = Constants._LOGIN_NO_EXISTE;
                            oResponse.developer = Constants._LOGIN_NO_EXISTE_DEV;
                        }
                    }
                    catch (Exception ex)
                    {
                        oResponse.message = Constants._GENERAL_ERROR;
                        oResponse.developer = String.Format(Constants._GENERAL_ERROR_DEV, ex.Message);
                    }
                }
            }
            else
            {
                oResponse.message = result.message;
                oResponse.developer = result.developer;
            }
            return oResponse;
        }
    }
}
