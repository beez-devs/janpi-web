﻿using JAMPI.Application.Contracts.Requests;
using JAMPI.Domain.Commons;
using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JAMPI.Application.Interfaces.Services
{
  public  interface IPersonaService
    {
        Task<Response<int>> InsertarAsync(Persona persona);
    }
}
