﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Interfaces.Repositories
{
    public interface IUnitOfWork
    {
        IUsuarioRepository usuarioRepository { get; }
        IPersonaRepository personaRepository { get; }
    }
}
