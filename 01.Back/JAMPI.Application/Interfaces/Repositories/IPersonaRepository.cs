﻿using JAMPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application.Interfaces.Repositories
{
   public interface IPersonaRepository : IGenericRepository<Persona>
    {
    }
}
