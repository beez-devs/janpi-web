﻿using JAMPI.Application.ExternalServices.Interfaces;
using JAMPI.Application.ExternalServices.Services;
using JAMPI.Application.Interfaces.Services;
using JAMPI.Application.Structure.UsuarioStructure.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace JAMPI.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<IGoogle, Google>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IPersonaService, PersonaService>();
            return services;
        }
    }
}
